'use strict';

module.exports = function(Employee) {
  Employee.beforeRemote('create', function(context, user, next) {
    context.args.data.created = Date.now();
    next();
  });
};
